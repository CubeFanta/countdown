const daysEl = document.getElementById('days')
const hoursEl = document.getElementById('hours')
const minsEl = document.getElementById('mins')
const secondEl = document.getElementById('seconds')


function countdown() {
	const currentDate = new Date();
	const newYearsDate = new Date(currentDate.getFullYear() + 1, 0, 1)
	const totalSecond = (newYearsDate - currentDate) / 1000;

	const days = Math.floor(totalSecond / 3600 / 24)
	const hours = Math.floor(totalSecond / 3600) % 24;
	const mins = Math.floor(totalSecond / 60) % 60;
	const second = Math.floor(totalSecond % 60);

	daysEl.innerHTML = days;
	hoursEl.innerHTML = hours;
	minsEl.innerHTML = mins;
	secondEl.innerHTML = second;
};

countdown()

setInterval(countdown, 1000)